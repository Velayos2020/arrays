***Settings***
Documentation   Prueba Regresiva Automática Contenedores
Library     SeleniumLibrary
***Variables***
${Browser}      chrome
${TestUrl}      http
${HomePage}     automationpractice.com/index.php
${TextUrl }     ${TestUrl}://${HomePage}

***Keywords***
Open Homepage
    Open Browser        ${TextUrl }     ${Browser}
***Test Cases***
C001 Hacer click en contenedores
    Open Homepage
    Set Global Variable     @{NombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR     ${NombreDeContenedor}   IN      @{NombresDeContenedores}
    \   Run Keyword If      '${NombreDeContenedor}'=='//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a'     Exit for Loop
    \   Click Element       xpath=${NombreDeContenedor}
    \   Wait until Element is Visible       xpath=//*[@id="bigpic"]
    \   Click Element       xpath=//*[@id="header_logo"]/a/img
    Close Browser